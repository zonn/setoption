package com.setoption;

import com.setoption.utils.JwtUtil;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import tk.mybatis.spring.annotation.MapperScan;


@SpringBootApplication
@MapperScan("com.setoption.mapper")
public class SetoptionApplication {

    public static void main(String[] args) {
        SpringApplication.run(SetoptionApplication.class, args);
    }
    @Bean
    public JwtUtil jwtUtil(){return new JwtUtil();}
}
