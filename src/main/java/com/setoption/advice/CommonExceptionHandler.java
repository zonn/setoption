package com.setoption.advice;

import com.setoption.entity.ExceptionResult;
import com.setoption.exception.MyException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
//拦截所有controller
public class CommonExceptionHandler {
    @ExceptionHandler(MyException.class)
    public ResponseEntity<ExceptionResult> handleException(MyException e){
        return ResponseEntity.ok(new ExceptionResult(e.getExceptionEnums()));
    }
}
