package com.setoption.config;

import com.setoption.interceptor.ManagerInterceptor;
import com.setoption.interceptor.StudentInterceptor;
import com.setoption.interceptor.TeacherInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.CacheControl;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

@Configuration
public class MyConfig extends WebMvcConfigurationSupport {
    @Autowired
    private StudentInterceptor studentInterceptor;
    @Autowired
    private TeacherInterceptor teacherInterceptor;
    @Autowired
    private ManagerInterceptor managerInterceptor;


    @Override
    protected void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(managerInterceptor).addPathPatterns("/manager/**");
        registry.addInterceptor(teacherInterceptor).addPathPatterns("/teacher/**");
        registry.addInterceptor(studentInterceptor).addPathPatterns("/student/**");
    }

    @Override
    protected void addResourceHandlers(ResourceHandlerRegistry registry) {

    }
}
