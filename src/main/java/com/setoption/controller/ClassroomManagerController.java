package com.setoption.controller;


import com.setoption.entity.ReturnInfo;
import com.setoption.enums.StatusCode;
import com.setoption.service.ClassroomManagerService;
import com.setoption.service.StudentManagerService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Map;

@RestController
public class ClassroomManagerController {
    @Resource
    private ClassroomManagerService managerService;

    @GetMapping("/manager/classroom/select")
    public ReturnInfo selectAllClassroom(@RequestParam Map map){
        return new ReturnInfo(StatusCode.OK,"所有教室查询完成", managerService.selectAllClassroom(map));
    }

    @PostMapping("/manager/classroom/add")
    public ReturnInfo addClassroom(@RequestBody Map<String, Object> map){
        return new ReturnInfo(StatusCode.OK,"教室添加成功", managerService.addClassroom(map));
    }

    @PostMapping("/manager/classroom/delete")
    public ReturnInfo delClassroom(@RequestBody Map<String, Object> map){
        return new ReturnInfo(StatusCode.OK,"删除教室完成", managerService.delClassroom(map));
    }

    @PostMapping("/manager/classroom/edit")
    public ReturnInfo setClassroomInfo(@RequestBody Map<String, Object> map){
        return new ReturnInfo(StatusCode.OK,"教室信息修改完成", managerService.setClassroomInfo(map));
    }

}
