package com.setoption.controller;

import com.setoption.entity.ReturnInfo;
import com.setoption.enums.StatusCode;
import com.setoption.service.ClassroomSetService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Map;
@RestController
public class ClassroomSetController {
    @Resource
    private ClassroomSetService classroomSetService;
    @GetMapping("/manager/allot")
    public ReturnInfo setClassroom(){
        return new ReturnInfo(StatusCode.OK,"教室分配完成", classroomSetService.setClassroom());
    }

    @PostMapping("/manager/fenpei")
    public ReturnInfo fenpeiClassroom(@RequestBody Map<String, Object> map){
        return new ReturnInfo(StatusCode.OK,"分配结束", classroomSetService.fenpeiAganin(map));
    }
}
