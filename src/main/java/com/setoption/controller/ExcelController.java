package com.setoption.controller;


import com.alibaba.excel.EasyExcel;
import com.setoption.entity.ReturnInfo;
import com.setoption.enums.StatusCode;
import com.setoption.listener.ClassroomListener;
import com.setoption.listener.StudentListener;
import com.setoption.listener.TeacherListener;
import com.setoption.pojo.Classroom;
import com.setoption.pojo.Students;
import com.setoption.pojo.Teachers;
import com.setoption.service.ClassroomManagerService;
import com.setoption.service.StudentsService;
import com.setoption.service.TeacherManagerService;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.IOException;
@RestController
public class ExcelController {
    @Resource
    private StudentsService studentService;
    @Resource
    private TeacherManagerService teacherService;
    @Resource
    private ClassroomManagerService classroomService;



    @PostMapping("/manager/upload/student")
    public ReturnInfo uploadStudent(MultipartFile file) throws IOException {
        EasyExcel.read(file.getInputStream(), Students.class, new StudentListener(studentService)).sheet().doRead();
        return new ReturnInfo(StatusCode.OK,"上传成功","student");
    }
    @PostMapping("/manager/upload/teacher")
    public ReturnInfo uploadTeacher(MultipartFile file) throws IOException {
        EasyExcel.read(file.getInputStream(), Teachers.class, new TeacherListener(teacherService)).sheet().doRead();
        return new ReturnInfo(StatusCode.OK,"上传成功","teacher");
    }
    @PostMapping("/manager/upload/classroom")
    public ReturnInfo uploadClassroom(MultipartFile file) throws IOException {
        EasyExcel.read(file.getInputStream(), Classroom.class, new ClassroomListener(classroomService)).sheet().doRead();
        return new ReturnInfo(StatusCode.OK,"上传成功","classroom");
    }

}
