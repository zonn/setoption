package com.setoption.controller;

import com.setoption.entity.ReturnInfo;
import com.setoption.enums.StatusCode;
import com.setoption.service.InfoManagerService;
import com.setoption.service.StudentManagerService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Map;
@RestController
public class InfoManagerController {
    @Resource
    private InfoManagerService managerService;

    @GetMapping("/manager/select")
    public ReturnInfo selectManager(HttpServletRequest request){
        return new ReturnInfo(StatusCode.OK,"管理员信息查询完成", managerService.selectManager(request));
    }

    @PostMapping("/manager/edit")
    public ReturnInfo setManagerInfo(@RequestBody Map<String, Object> map){
        return new ReturnInfo(StatusCode.OK,"管理员信息修改完成", managerService.setManagerInfo(map));
    }
}
