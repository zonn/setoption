package com.setoption.controller;

import com.setoption.entity.ReturnInfo;
import com.setoption.enums.StatusCode;
import com.setoption.service.NewTermService;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Map;
@RestController
public class NewTermController {

    @Resource
    private NewTermService newTermService;

    @PostMapping("/manager/new")
    public ReturnInfo openNewTerm(@RequestBody Map<String, Object> map) {
        return new ReturnInfo(StatusCode.OK, "新学期开始", newTermService.openNewTerm(map));
    }

}
