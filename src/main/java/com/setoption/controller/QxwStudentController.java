package com.setoption.controller;

import com.setoption.entity.ReturnInfo;
import com.setoption.enums.StatusCode;
import com.setoption.service.StudentsService;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Map;
@RestController
public class QxwStudentController {

  @Resource
  private StudentsService studentsService;




  @PostMapping("/student/update")
  public ReturnInfo updateStudent(@RequestBody Map<String, Object> map){
    return new ReturnInfo(StatusCode.OK,"信息修改成功",studentsService.update_Stu(map));
  }
  @PostMapping("/student/getCourseArrangement")
  public ReturnInfo selectCourseArrangement(@RequestBody Map<String, Object> map){
    return new ReturnInfo(StatusCode.OK,"学生课表查询",studentsService.getCourseArrangement(map));
  }

//选课成功
  @PostMapping("/student/class/add")
  public ReturnInfo chooseCourse(@RequestBody Map<String, Object> map){
    return new ReturnInfo(StatusCode.OK,"选课成功",studentsService.chooseCourses(map));
  }



  @PostMapping("/student/score")
  public ReturnInfo selectScore(@RequestBody Map<String, Object> map){
    return new ReturnInfo(StatusCode.OK,"查询成绩成功",studentsService.selectScore(map));
  }



  @PostMapping("/student/select")
  public ReturnInfo studentInfo(@RequestBody Map<String, Object> map){
    return new ReturnInfo(StatusCode.OK,"查询信息成功",studentsService.studentInfo(map));
  }




  @PostMapping("/student/class/delete")
  public ReturnInfo delCourseSelection(@RequestBody Map<String, Object> map){
    ReturnInfo a =  new ReturnInfo(StatusCode.OK,"退选成功",studentsService.deleteCourseSelection(map)) ;
    return a;
  }



  /**
   * 查
   * @return
   */
  @PostMapping("/student/course")
  public ReturnInfo courseInfo(){
    return  new ReturnInfo(StatusCode.OK,"课程信息",studentsService.courseInfo());
  }

  @PostMapping("/student/uppassword")
  public ReturnInfo updateStuPassword(@RequestBody Map<String, Object> map){
    return new ReturnInfo(StatusCode.OK,"密码修改成功",studentsService.updateStuPassword(map));
  }

//  选课查询成功提示

  @PostMapping("/student/class/select")
  public ReturnInfo studentClassSelect(@RequestBody Map<String, Object> map){
    return new ReturnInfo(StatusCode.OK,"已选课程查询成功",studentsService.studentClassSelect(map));
  }



}
