package com.setoption.controller;

import com.setoption.entity.ReturnInfo;
import com.setoption.enums.StatusCode;
import com.setoption.service.StudentManagerService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Map;
@RestController
public class StudentManagerController {
    @Resource
    private StudentManagerService managerService;

    //管理员查询学生
    @GetMapping("/manager/student/select")
    public ReturnInfo selectAllStu(@RequestParam Map map){
        return new ReturnInfo(StatusCode.OK,"所有学生查询完成", managerService.selectAllStu(map));
    }

    @PostMapping("/manager/student/add")
    public ReturnInfo addStudent(@RequestBody Map<String, Object> map){
        return new ReturnInfo(StatusCode.OK,"学生添加成功", managerService.addStudent(map));
    }

    @PostMapping("/manager/student/reset")
    public ReturnInfo resetStuPassword(@RequestBody Map<String, Object> map){
        return new ReturnInfo(StatusCode.OK,"重置学生密码查询完成", managerService.resetStuPassword(map));
    }

    @PostMapping("/manager/student/delete")
    public ReturnInfo delStu(@RequestBody Map<String, Object> map){
        return new ReturnInfo(StatusCode.OK,"删除学生完成", managerService.delStu(map));
    }


}
