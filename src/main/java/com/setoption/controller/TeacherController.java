package com.setoption.controller;

import com.setoption.entity.ReturnInfo;
import com.setoption.enums.StatusCode;
import com.setoption.service.TeachersService;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Map;

@RestController
public class TeacherController {
    @Resource
    private TeachersService teachersService;

    @PostMapping("/teacher/update")
    public ReturnInfo updateTeacher(@RequestBody Map<String, Object> map){
        return new ReturnInfo(StatusCode.OK,"信息修改成功",teachersService.update_Tea(map));
    }

    @PostMapping("/teacher/setscore")
    public ReturnInfo setStuScore(@RequestBody Map<String, Object> map){
        return new ReturnInfo(StatusCode.OK,"设置分数成功",teachersService.setStudentScore(map));
    }

    @PostMapping("/teacher/CourseSelection")
    public ReturnInfo stuCourseSelection(@RequestBody Map<String, Object> map){
        return new ReturnInfo(StatusCode.OK,"查看选课情况成功",teachersService.stuCourseSelection(map));
    }

    @PostMapping("/teacher/course/add")
    public ReturnInfo addCourse(@RequestBody Map<String, Object> map){
        return new ReturnInfo(StatusCode.OK,"添加课程成功",teachersService.addCourse(map));
    }

    @PostMapping("/teacher/select")
    public ReturnInfo teacherInfo(@RequestBody Map<String, Object> map){
        return new ReturnInfo(StatusCode.OK,"查询信息成功",teachersService.teacherInfo(map));
    }

    @PostMapping("/teacher/uppassword")
    public ReturnInfo updateTeaPassword(@RequestBody Map<String, Object> map){
        return new ReturnInfo(StatusCode.OK,"密码修改成功",teachersService.updateTeaPassword(map));
    }

    @PostMapping("/teacher/class/select")
    public ReturnInfo teacherCourse(@RequestBody Map<String, Object> map){
        return new ReturnInfo(StatusCode.OK,"查询授课成功",teachersService.teacherCourse(map));
    }

    @PostMapping("/teacher/class/arrange")
    public ReturnInfo teaCourseArrangement(@RequestBody Map<String, Object> map){
        return new ReturnInfo(StatusCode.OK,"查询教师课程安排成功",teachersService.teaCourseArrangement(map));
    }

    @PostMapping("/teacher/student/list")
    public ReturnInfo studentCourseList(@RequestBody Map<String, Object> map){
        return new ReturnInfo(StatusCode.OK,"本课程学生信息",teachersService.studentCourseList(map));
    }
}
