package com.setoption.controller;

import com.setoption.entity.ReturnInfo;
import com.setoption.enums.StatusCode;
import com.setoption.service.TeacherManagerService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Map;
@RestController
public class TeacherManagerController {
    @Resource
    private TeacherManagerService managerService;
//    管理员教师查询
    @GetMapping ("/manager/teacher/select")
    public ReturnInfo selectAllTea(@RequestParam Map map){
        return new ReturnInfo(StatusCode.OK,"所有教师查询完成", managerService.selectAllTea(map));
    }
//    管理员添加教师
    @PostMapping ("/manager/teacher/add")
    public ReturnInfo addTeacher(@RequestBody Map<String, Object> map){
        return new ReturnInfo(StatusCode.OK,"教师添加成功", managerService.addTeacher(map));
    }
    //删除教师信息
    @PostMapping("/manager/teacher/delete")
    public ReturnInfo delTea(@RequestBody Map<String, Object> map){
        return new ReturnInfo(StatusCode.OK,"删除教师完成", managerService.delTea(map));
    }

    @PostMapping("/manager/teacher/reset")
    public ReturnInfo resetTeaPassword(@RequestBody Map<String, Object> map){
        return new ReturnInfo(StatusCode.OK,"重置教师密码完成", managerService.resetTeaPassword(map));
    }
}
