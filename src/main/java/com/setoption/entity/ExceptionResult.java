package com.setoption.entity;

import com.setoption.enums.ExceptionEnums;
import lombok.Data;

@Data
public class ExceptionResult {
    private int status ;
    private String msg ;
    private Long timestamp;

    public ExceptionResult(ExceptionEnums em ) {
        this.status=em.getCode();
        this.msg = em.getMsg();
        this.timestamp = System.currentTimeMillis();
    }
}
