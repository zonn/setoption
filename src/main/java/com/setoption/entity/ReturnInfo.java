package com.setoption.entity;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ReturnInfo {
    private Integer status ;
    private String message ;
    private Object data;

    public ReturnInfo(Integer code, String message) {
        this.status = code;
        this.message = message;
    }


}
