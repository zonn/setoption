package com.setoption.entity;

import com.setoption.pojo.CourseArrangement;
import com.setoption.pojo.CourseInfo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SetClassroomSurplus {
    private String message;
    private Map<String,Integer> classroom;
    private List<CourseInfo> courseInfos;
}
