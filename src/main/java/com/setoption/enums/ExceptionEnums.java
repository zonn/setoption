package com.setoption.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public enum ExceptionEnums {
    NO_PERMISSIONS(403,"没有权限"),
    TOKEN_TIME_OUT(403,"无效token"),
    PARAMETER_ERROR(400,"参数错误"),
    PARAMETER_ALREADY_EXIST(400,"记录已存在"),
    USERNAME_OR_PASSWORD_ERROR(400,"用户名或密码错误"),
    USER_ALREADY_EXIST(400,"用户已存在"),
    TABLE_INFO_ERROR(400,"上传表格信息有误"),
    TABLE_SET_ERROR(400,"数据库操作出错"),
    ;
    private Integer code;
    private String msg;
}
