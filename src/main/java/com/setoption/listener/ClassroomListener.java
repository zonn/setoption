package com.setoption.listener;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.alibaba.fastjson.JSON;
import com.setoption.mapper.ClassroomManagerMapper;
import com.setoption.pojo.Classroom;
import com.setoption.service.ClassroomManagerService;
import org.ehcache.core.events.CacheManagerListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class ClassroomListener extends AnalysisEventListener<Classroom> {
    private static final Logger LOGGER = LoggerFactory.getLogger(ClassroomListener.class);

    private static final int BATCH_COUNT = 3000;
    List<Classroom> list = new ArrayList<Classroom>();
    /**
     * 假设这个是一个DAO，当然有业务逻辑这个也可以是一个service。当然如果不用存储这个对象没用。
     */
    private ClassroomManagerService classroomManagerService;
    public ClassroomListener(ClassroomManagerService classroomManagerService) {
        // 这里是demo，所以随便new一个。实际使用如果到了spring,请使用下面的有参构造函数
        this.classroomManagerService=classroomManagerService;
    }
    @Override
    public void invoke(Classroom data, AnalysisContext context) {

        LOGGER.info("解析到一条数据:{}", JSON.toJSONString(data));
        list.add(data);
        // 达到BATCH_COUNT了，需要去存储一次数据库，防止数据几万条数据在内存，容易OOM
        if (list.size() >= BATCH_COUNT) {
            saveData();
            // 存储完成清理 list
            list.clear();
        }

    }

    @Override
    public void doAfterAllAnalysed(AnalysisContext context) {
        // 这里也要保存数据，确保最后遗留的数据也存储到数据库
        saveData();
        LOGGER.info("所有数据解析完成！");
    }
    private void saveData() {
        LOGGER.info("{}条数据，开始存储数据库！", list.size());
        classroomManagerService.save(list);
        LOGGER.info("存储数据库成功！");
    }
}
