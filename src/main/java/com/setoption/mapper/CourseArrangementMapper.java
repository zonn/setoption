package com.setoption.mapper;

import com.setoption.pojo.CourseArrangement;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import tk.mybatis.mapper.common.ConditionMapper;
import tk.mybatis.mapper.common.IdsMapper;
import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.mapper.common.MySqlMapper;

import java.util.List;

public interface CourseArrangementMapper extends Mapper<CourseArrangement>, MySqlMapper<CourseArrangement>, ConditionMapper<CourseArrangement>, IdsMapper<CourseArrangement> {
    @Select("select * from course_arrangement where classroom_number = #{classroomNumber}")
    public List<CourseArrangement> getSetedClassroomByCNum(@Param("classroomNumber") String classroomNumber);

    @Select("select * from ${year}${term}course_arrangement")
    public List<CourseArrangement> selectAllCourseArrangement(@Param("year") String year,@Param("term") String term);

    @Select("select * from ${year}${term}course_arrangement where id = #{id}")
    public CourseArrangement selectClassroomById(@Param("id") Integer id,@Param("year") String year,@Param("term") String term);

    @Update("update ${year}${term}course_arrangement set classroom_number=#{classroomNo}, course_time=#{courseTime}  where id = #{id}")
    public int updateClassroomById(@Param("id") Integer id,@Param("classroomNo") String classroomNo,@Param("courseTime") Integer courseTime,@Param("year") String year,@Param("term") String term);


    @Insert ({"<script>" +
            "<foreach collection='CourseArrangementList' item='item' separator=';'>" +
            " INSERT INTO" +
            " ${year}${term}course_arrangement" +
            " (course_time,classroom_number,course_classno,course_num)" +
            " VALUES("+
            " #{item.courseTime, jdbcType=INTEGER}, " +
            " #{item.classroomNumber, jdbcType=VARCHAR} ," +
            " #{item.courseClassno, jdbcType=VARCHAR} ," +
            " #{item.courseNum, jdbcType=VARCHAR} )" +
            "</foreach>" +
            "</script>"})
    int batchInsert(@Param("CourseArrangementList")List<CourseArrangement>list,@Param("year") String year,@Param("term")String term);




    @Update({"<script>" +
            "<foreach collection='CourseArrangementList' item='item' separator=';'>" +
            " UPDATE" +
            " ${year}${term}course_arrangement" +
            " SET course_time = #{item.courseTime, jdbcType=INTEGER}, " +
            "  classroom_number = #{item.classroomNumber, jdbcType=VARCHAR} " +
            "  WHERE " +
            "  id = #{item.id, jdbcType=INTEGER} " +
            "</foreach>" +
            "</script>"})
    int batchUpdate(@Param("CourseArrangementList")List<CourseArrangement>list,@Param("year") String year,@Param("term")String term);

    @Update("DROP TABLE IF EXISTS `${year}${term}course_arrangement`;" +
            "CREATE TABLE `${year}${term}course_arrangement`  (" +
            "  `id` int(11) NOT NULL AUTO_INCREMENT," +
            "  `course_num` varchar(12) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL," +
            "  `course_classno` varchar(12) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL," +
            "  `classroom_number` varchar(12) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL," +
            "  `course_time` int(2) NULL DEFAULT NULL," +
            "  PRIMARY KEY (`id`) USING BTREE" +
            ") ")
    int createTable(@Param("year") String year,@Param("term")String term);

}