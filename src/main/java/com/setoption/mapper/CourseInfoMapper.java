package com.setoption.mapper;

import com.setoption.pojo.CourseArrangement;
import com.setoption.pojo.CourseInfo;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import tk.mybatis.mapper.common.ConditionMapper;
import tk.mybatis.mapper.common.IdsMapper;
import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.mapper.common.MySqlMapper;

import java.util.List;

public interface CourseInfoMapper extends Mapper<CourseInfo>, MySqlMapper<CourseInfo>, ConditionMapper<CourseInfo>, IdsMapper<CourseInfo> {
    @Select("select * from ${year}${term}course_info where course_no=${courseNo}")
    public CourseInfo selectCourseByCourseNo(@Param("courseNo") String courseNo, @Param("year") String year, @Param("term") String term);

    @Select("select * from ${year}${term}course_info")
    public List<CourseInfo> selectAllCourseInfo(@Param("year") String year, @Param("term") String term);


    @Update("DROP TABLE IF EXISTS `${year}${term}course_info`;" +
            "CREATE TABLE `${year}${term}course_info`  (" +
            "  `id` int(11) NOT NULL AUTO_INCREMENT," +
            "  `course_no` varchar(12) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL," +
            "  `teacher_number` varchar(12) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL," +
            "  `course_name` varchar(12) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL," +
            "  `course_object` varchar(12) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL," +
            "  `course_people` int(3) NULL DEFAULT NULL," +
            "  `course_multimedia` tinyint(1) NULL DEFAULT NULL," +
            "  `course_credit` int(2) NULL DEFAULT NULL," +
            "  PRIMARY KEY (`id`) USING BTREE," +
            "  UNIQUE INDEX `onlyone`(`course_no`) USING BTREE" +
            ") ")
    int createTable(@Param("year") String year, @Param("term") String term);
}
