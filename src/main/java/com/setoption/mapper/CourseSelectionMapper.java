package com.setoption.mapper;

import com.setoption.pojo.CourseSelection;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import tk.mybatis.mapper.common.ConditionMapper;
import tk.mybatis.mapper.common.IdsMapper;
import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.mapper.common.MySqlMapper;

import java.util.List;

public interface CourseSelectionMapper extends Mapper<CourseSelection>, MySqlMapper<CourseSelection>, ConditionMapper<CourseSelection>, IdsMapper<CourseSelection> {
    @Select("select * from ${year}${term}course_selection where student_number like '${studentNumber}%'")
    public List<CourseSelection> selectByStudentNumber(@Param("studentNumber") String studentNumber, @Param("year") String year, @Param("term") String term);

    @Select("select * from ${year}${term}course_selection where course_number=#{num}")
    public List<CourseSelection> selectAllSelect(@Param("year") String year, @Param("term") String term,@Param("num") String num);

    @Update("DROP TABLE IF EXISTS `${year}${term}course_selection`;" +
            "CREATE TABLE `${year}${term}course_selection`  (" +
            "  `id` int(11) NOT NULL AUTO_INCREMENT," +
            "  `student_number` varchar(12) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL," +
            "  `course_number` varchar(12) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL," +
            "  `student_score` float NULL DEFAULT 0," +
            "  PRIMARY KEY (`id`) USING BTREE" +
            ") ")
    int createTable(@Param("year") String year, @Param("term") String term);
}
