package com.setoption.mapper;

import com.setoption.pojo.Managers;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import tk.mybatis.mapper.common.ConditionMapper;
import tk.mybatis.mapper.common.IdsMapper;
import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.mapper.common.MySqlMapper;

public interface InfoManagersMapper extends Mapper<Managers>, MySqlMapper<Managers>, ConditionMapper<Managers>, IdsMapper<Managers> {
    @Select("select * from managers where manager_no = #{userid} and manager_password = #{password}")
    Managers login(@Param("userid") String userid, @Param("password") String password);
}
