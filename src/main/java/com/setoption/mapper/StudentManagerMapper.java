package com.setoption.mapper;

import com.setoption.pojo.Students;
import tk.mybatis.mapper.common.ConditionMapper;
import tk.mybatis.mapper.common.IdsMapper;
import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.mapper.common.MySqlMapper;

public interface StudentManagerMapper extends Mapper<Students>, MySqlMapper<Students>, ConditionMapper<Students>, IdsMapper<Students> {
}
