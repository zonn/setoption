package com.setoption.mapper.qxw_mapper;

import com.setoption.pojo.Classroom;
import tk.mybatis.mapper.common.ConditionMapper;
import tk.mybatis.mapper.common.IdsMapper;
import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.mapper.common.MySqlMapper;

public interface ClassroomsMapper extends Mapper<Classroom>, MySqlMapper<Classroom>, ConditionMapper<Classroom>, IdsMapper<Classroom> {

}
