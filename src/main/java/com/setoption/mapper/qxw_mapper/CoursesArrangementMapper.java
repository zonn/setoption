package com.setoption.mapper.qxw_mapper;

import com.setoption.pojo.CourseArrangement;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import tk.mybatis.mapper.common.ConditionMapper;
import tk.mybatis.mapper.common.IdsMapper;
import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.mapper.common.MySqlMapper;

import java.util.List;
import java.util.Map;

public interface CoursesArrangementMapper extends Mapper<CourseArrangement>, MySqlMapper<CourseArrangement>, ConditionMapper<CourseArrangement>, IdsMapper<CourseArrangement> {
    @Select("select * from course_arrangement where classroom_number = #{classroomNumber}")
    public List<CourseArrangement> getSetedClassroomByCNum(@Param("classroomNumber") String classroomNumber);

    @Select("SELECT course_name,teacher_name,course_classno,classroom_number,course_time " +
            "FROM ${year}${term}course_arrangement,${year}${term}course_selection,${year}${term}course_info,teachers" +
            " WHERE ${year}${term}course_arrangement.course_num=${year}${term}course_selection.course_number " +
            " AND ${year}${term}course_selection.course_number=${year}${term}course_info.course_no" +
            " AND ${year}${term}course_info.teacher_number=teachers.teacher_no" +
            " AND ${year}${term}course_selection.student_number=#{studentNumber}")
    public List<Map<String,Object>> getCourseArrangement(@Param("year") String year, @Param("term") String term, @Param("studentNumber") String studentNumber);

    @Insert("insert into ${year}${term}course_arrangement(course_num,course_classno) values(#{courseNo},#{courseClassno})")
    void addCourseArr(@Param("year") String year, @Param("term") String term, @Param("courseNo") String courseNo, @Param("courseClassno") String courseClassno);

    @Select("select course_name,course_classno,classroom_number,course_time" +
            " from ${year}${term}course_info,${year}${term}course_arrangement,teachers" +
            " where ${year}${term}course_arrangement.course_num=${year}${term}course_info.course_no" +
            " and ${year}${term}course_info.teacher_number=teachers.teacher_no" +
            " and teachers.teacher_no=#{teacherNumber}")
    public List<Map<String,Object>> teaCourseArrangement(@Param("year") String year, @Param("term") String term, @Param("teacherNumber") String teacherNumber);
}
