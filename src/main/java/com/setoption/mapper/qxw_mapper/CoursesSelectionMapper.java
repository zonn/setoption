package com.setoption.mapper.qxw_mapper;

import com.setoption.pojo.CourseSelection;
import org.apache.ibatis.annotations.*;
import tk.mybatis.mapper.common.ConditionMapper;
import tk.mybatis.mapper.common.IdsMapper;
import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.mapper.common.MySqlMapper;

import java.util.List;
import java.util.Map;

public interface CoursesSelectionMapper extends Mapper<CourseSelection>, MySqlMapper<CourseSelection>, ConditionMapper<CourseSelection>, IdsMapper<CourseSelection> {
    @Select("select * from course_selection where student_number like '${studentNumber}%'")
    public List<CourseSelection> selectByStudentNumber(@Param("studentNumber") String studentNumber);

    @Update("update ${year}${term}course_selection set student_score=#{studentScore} where student_number = #{studentNumber} and course_number=#{courseNumber}")
    void setScore(@Param("year") String year, @Param("term") String term, @Param("studentNumber") String studentNumber, @Param("studentScore") Double studentScore, @Param("courseNumber") String courseNumber);

    @Insert("insert into ${year}${term}course_selection(student_number,course_number) values(#{studentNumber},#{courseNumber})")
    void chooseCourse(@Param("year") String year, @Param("term") String term, @Param("studentNumber") String studentNumber, @Param("courseNumber") String courseNumber);

    @Select("select course_name,student_score " +
            " from ${year}${term}course_info,${year}${term}course_selection,students " +
            " where ${year}${term}course_selection.course_number=${year}${term}course_info.course_no " +
            " and ${year}${term}course_selection.student_number=students.student_no " +
            " and students.student_no= #{studentNo} ")
    public List<Map<String,Object>> selectScore(@Param("year") String year, @Param("term") String term, @Param("studentNo") String studentNo);

    @Select("select course_no,course_name,student_no,student_name,student_sex " +
            "from ${year}${term}course_info,students,teachers,${year}${term}course_selection " +
            " where ${year}${term}course_selection.course_number=${year}${term}course_info.course_no " +
            " and ${year}${term}course_selection.student_number=students.student_no " +
            " and ${year}${term}course_info.teacher_number=teachers.teacher_no " +
            " and teachers.teacher_no=#{teacherNumber}")
    public List<Map<String,Object>> stuCourseSelection(@Param("year") String year, @Param("term") String term, @Param("teacherNumber") String teacherNumber);

    @Select("select course_no,course_name,teacher_name,course_credit " +
            " from ${year}${term}course_selection,${year}${term}course_info,students,teachers " +
            " where ${year}${term}course_selection.course_number=${year}${term}course_info.course_no " +
            " and ${year}${term}course_info.teacher_number=teachers.teacher_no" +
            " and ${year}${term}course_selection.student_number=students.student_no" +
            " and students.student_no = #{studentNo}")
    public List<Map<String,Object>> studentClassSelect(@Param("year") String year, @Param("term") String term, @Param("studentNo") String studentNo);

    @Select("select student_no,student_name,course_name,student_score" +
            " from ${year}${term}course_selection,${year}${term}course_info,students,teachers" +
            " where ${year}${term}course_selection.course_number=${year}${term}course_info.course_no" +
            " and ${year}${term}course_selection.student_number=students.student_no" +
            " and ${year}${term}course_info.teacher_number=teachers.teacher_no" +
            " and teachers.teacher_no=#{teacherNo}" +
            " and ${year}${term}course_selection.course_number=#{courseNo}")
    public List<Map<String,Object>> studentCourseList(@Param("year") String year, @Param("term") String term, @Param("teacherNo") String teacherNo, @Param("courseNo") String courseNo);

    @Delete("delete from ${year}${term}course_selection where student_number = #{studentNumber} and course_number =#{courseNumber}")
    void deleteCourseSelection(@Param("year") String year, @Param("term") String term, @Param("studentNumber") String studentNumber, @Param("courseNumber") String courseNumber);
}
