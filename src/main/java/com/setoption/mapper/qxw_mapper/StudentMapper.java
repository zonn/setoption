package com.setoption.mapper.qxw_mapper;
import com.setoption.pojo.Students;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import tk.mybatis.mapper.common.ConditionMapper;
import tk.mybatis.mapper.common.IdsMapper;
import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.mapper.common.MySqlMapper;

public interface StudentMapper extends Mapper<Students>, MySqlMapper<Students>, ConditionMapper<Students>, IdsMapper<Students> {
    @Select("select * from students where student_no = #{userid} and student_password = #{password}")
    Students login(@Param("userid") String userid, @Param("password") String password);

    @Update("update students set student_name=#{studentName},student_sex=#{studentSex},student_age=#{studentAge} where student_no=#{studentNo}")
    void updateStu(@Param("studentName") String studentName, @Param("studentSex") String studentSex, @Param("studentAge") Integer studentAge, @Param("studentNo") String studentNo);

    @Select("select student_no,student_name,student_sex,student_age from students where student_no=#{studentNo}")
    Students studentInfo(@Param("studentNo") String studentNo);

    @Update("update students set student_password = #{studentNewPassword} where student_no=#{studentNo} and student_password=#{studentPassword}")
    void updatePassword(@Param("studentPassword") String studentPassword, @Param("studentNewPassword") String studentNewPassword, @Param("studentNo") String studentNo);

}
