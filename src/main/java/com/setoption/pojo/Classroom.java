package com.setoption.pojo;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Id;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Classroom {
    @Id
    @ExcelIgnore
    private Integer id;
    @ExcelProperty("教室编号")
    private String classroomNo;
    @ExcelProperty("教室是否支持多媒体")
    private Boolean classroomMutil;
    @ExcelProperty("教室可容纳人数")
    private Integer classroomCapacity;
    @ExcelProperty("教室楼层")
    private String classroomLevel;
    @ExcelProperty("所在教学楼")
    private String classroomBuild;

    public Classroom(String classroomNo, Boolean classroomMutil, Integer classroomCapacity,String classroomLevel,String classroomBuild) {
        this.classroomNo = classroomNo;
        this.classroomMutil = classroomMutil;
        this.classroomCapacity = classroomCapacity;
        this.classroomLevel = classroomLevel;
        this.classroomBuild = classroomBuild;
    }

}
