package com.setoption.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Id;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CourseArrangement {
    @Id
    private Integer id;
    private String courseNum;
    private String courseClassno;
    private String classroomNumber;
    private Integer courseTime;
}
