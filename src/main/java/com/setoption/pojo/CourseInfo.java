package com.setoption.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Id;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CourseInfo {
    @Id
    private Integer id;
    private String courseNo;
    private String teacherNumber;
    private String courseName;
    private String courseObject;
    private Integer coursePeople;
    private Boolean courseMultimedia;
    private Integer courseCredit;

}
