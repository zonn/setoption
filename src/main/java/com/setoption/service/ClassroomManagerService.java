package com.setoption.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.setoption.entity.PageResult;
import com.setoption.enums.ExceptionEnums;
import com.setoption.exception.MyException;
import com.setoption.mapper.*;
import com.setoption.pojo.*;
import io.jsonwebtoken.Claims;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.util.*;

@Service
public class ClassroomManagerService {

    @Resource
    private ClassroomManagerMapper classroomManagerMapper;

    public int addClassroom(Map<String, Object> map) {
        Boolean bol= String.valueOf(map.get("classroom_mutil")).equals("true");
        Classroom classroom = new Classroom(String.valueOf(map.get("classroom_no")), bol, Integer.parseInt(map.get("classroom_capacity").toString()),String.valueOf(map.get("classroom_level")),String.valueOf(map.get("classroom_build")));

        return classroomManagerMapper.insertSelective(classroom);
    }


    public List<Classroom> selectAllClassroom() {
        return classroomManagerMapper.selectAll();
    }

    public PageResult<Classroom> selectAllClassroom(Map<String, Object> map) {
        Integer pageNum = Integer.valueOf(map.get("page").toString());
        Integer pageSize = Integer.valueOf(map.get("limit").toString());
        PageHelper.startPage(pageNum, pageSize);
        Example example = new Example(Classroom.class);
        List<Classroom> classrooms = classroomManagerMapper.selectByExample(example);
        PageInfo<Classroom> userPageInfo = new PageInfo<Classroom>(classrooms);
        return new PageResult<Classroom>(userPageInfo.getTotal(), userPageInfo.getList());
    }


    public int delClassroom(Map<String, Object> map) {
        int result = classroomManagerMapper.deleteByIds(map.get("id").toString());
        if (result == 0) {
            throw new MyException(ExceptionEnums.TABLE_SET_ERROR);
        }
        return result;
    }


    public int setClassroomInfo(Map<String, Object> map){
        String classroom_no=String.valueOf(map.get("classroom_no"));
        Boolean classroom_mutil=String.valueOf(map.get("classroom_mutil")).equals("true");;
        Integer classroom_capacity=Integer.parseInt(map.get("classroom_capacity").toString());
        String classroom_build=String.valueOf(map.get("classroom_build"));
        String classroom_level=String.valueOf(map.get("classroom_level"));
        Classroom classroom=new Classroom();
        Integer id=Integer.parseInt( map.get("id").toString());
        classroom.setId(id);
        classroom.setClassroomNo(classroom_no);
        classroom.setClassroomMutil(classroom_mutil);
        classroom.setClassroomCapacity(classroom_capacity);
        classroom.setClassroomBuild(classroom_build);
        classroom.setClassroomLevel(classroom_level);
        int result=classroomManagerMapper.updateByPrimaryKeySelective(classroom);
        if(result==0){
            throw new MyException(ExceptionEnums.TABLE_SET_ERROR);
        }
        return result;
    }

    public void save(List<Classroom> list) {
        try {
            classroomManagerMapper.insertList(list);
        } catch (Exception e) {
            e.printStackTrace();
            throw new MyException(ExceptionEnums.TABLE_INFO_ERROR);
        }
    }
    }


