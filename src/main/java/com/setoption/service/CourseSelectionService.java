package com.setoption.service;

import com.setoption.entity.Term;
import com.setoption.mapper.CourseSelectionMapper;
import com.setoption.pojo.CourseSelection;
import com.setoption.utils.JsonUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.File;
import java.util.List;
import java.util.Map;

@Service
public class CourseSelectionService {
    @Resource
    private CourseSelectionMapper courseSelectionMapper;
    @Value("${myconfig.filepath}")
    private File systemSetting;
    @Resource
    private JsonUtils jsonUtils;

    public int createTable(Map<String, Object> map){
        String year=String.valueOf(map.get("year"));
        String term=String.valueOf(map.get("term"));
        int result=courseSelectionMapper.createTable(year,term);
       /* if (result==0)
            throw new MyException(ExceptionEnums.TABLE_SET_ERROR);*/
        return result;
    }

    public List<CourseSelection>getCourseSelectionByStudentNumber(String studentNumber){
        Term term=jsonUtils.readJson(systemSetting,Term.class);
        return courseSelectionMapper.selectByStudentNumber(studentNumber,term.getYear(),term.getTerm());
    }
    public int getCourseSelectionSum(String num){
        Term term=jsonUtils.readJson(systemSetting,Term.class);
        return courseSelectionMapper.selectAllSelect(term.getYear(),term.getTerm(),num).size();
    }
}
