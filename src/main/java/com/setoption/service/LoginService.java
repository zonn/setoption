package com.setoption.service;

import com.setoption.enums.ExceptionEnums;
import com.setoption.exception.MyException;
import com.setoption.mapper.qxw_mapper.ManagerMapper;
import com.setoption.mapper.qxw_mapper.StudentMapper;
import com.setoption.mapper.qxw_mapper.TeacherMapper;
import com.setoption.pojo.Managers;
import com.setoption.pojo.Students;
import com.setoption.pojo.Teachers;
import com.setoption.utils.JwtUtil;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Map;

@Service
public class LoginService {
    @Resource
    private ManagerMapper managersMapper;
    @Resource
    private TeacherMapper teachersMapper;
    @Resource
    private StudentMapper studentsMapper;

    @Resource
    private JwtUtil jwtUtil;

    public String mangerLogin(Map<String, Object> map) {
        Managers manager =
                managersMapper.login(String.valueOf(map.get("username")), String.valueOf(map.get("password")));
        if (manager==null){
            throw new MyException(ExceptionEnums.USERNAME_OR_PASSWORD_ERROR);
        }else {
            String token=jwtUtil.createJWT(manager.getManagerNo()+"","manager");
            return token;
        }
    }
    public String studentLogin(Map<String, Object> map) {
        Students students =
                studentsMapper.login(String.valueOf(map.get("username")), String.valueOf(map.get("password")));
        if (students==null){
            throw new MyException(ExceptionEnums.USERNAME_OR_PASSWORD_ERROR);
        }else {
            String token=jwtUtil.createJWT(students.getStudentNo()+"","student");
            return token;
        }
    }
    public String teacherLogin(Map<String, Object> map) {
        Teachers teachers =
                teachersMapper.login(String.valueOf(map.get("username")), String.valueOf(map.get("password")));
        if (teachers==null){
            throw new MyException(ExceptionEnums.USERNAME_OR_PASSWORD_ERROR);
        }else {
            String token=jwtUtil.createJWT(teachers.getTeacherNo()+"","teacher");
            return token;
        }
    }

}
