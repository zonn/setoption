package com.setoption.service;

import com.setoption.entity.Term;
import com.setoption.utils.JsonUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.File;
import java.util.Map;

@Service
public class NewTermService {
    @Value("${myconfig.filepath}")
    private File systemSetting;
    @Resource
    private JsonUtils jsonUtils;
    @Resource
    private ClassroomSetService classroomSetService;
    @Resource
    private CourseInfoService courseInfoService;
    @Resource
    private CourseSelectionService courseSelectionService;
    public int openNewTerm(Map<String, Object> map){
        classroomSetService.createTable(map);
        courseSelectionService.createTable(map);
        courseInfoService.createTable(map);
        Term term=new Term(String.valueOf(map.get("year")),String.valueOf(map.get("term")));
        jsonUtils.writeJson(systemSetting,term);
        return 1;
    }
}
