package com.setoption.service;

import com.setoption.entity.Term;
import com.setoption.enums.ExceptionEnums;
import com.setoption.exception.MyException;
import com.setoption.mapper.qxw_mapper.StudentMapper ;
import com.setoption.mapper.qxw_mapper.CoursesArrangementMapper;
import com.setoption.mapper.qxw_mapper.CoursesInfoMapper;
import com.setoption.mapper.qxw_mapper.CoursesSelectionMapper;
import com.setoption.pojo.Students;
import com.setoption.utils.JsonUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.File;
import java.util.List;
import java.util.Map;

@Service
public class StudentsService {
    @Value("${myconfig.filepath}")
    private File systemSetting;
    @Resource
    private JsonUtils jsonUtils;
    @Resource
    private StudentMapper studentMapper;
    @Resource
    private CoursesArrangementMapper coursesArrangementMapper;
    @Resource
    private CoursesSelectionMapper coursesSelectionMapper;
    @Resource
    private CoursesInfoMapper coursesInfoMapper;

    public String update_Stu(Map<String, Object> map){
        studentMapper.updateStu(String.valueOf(map.get("studentName")),String.valueOf(map.get("studentSex")),Integer.parseInt(map.get("studentAge").toString()),String.valueOf(map.get("studentNo")));
        String str = "学生信息更新成功" ;
        return str ;
    }

    public List<Map<String,Object>> getCourseArrangement(Map<String, Object> map){
        Term term=jsonUtils.readJson(systemSetting,Term.class);
        return coursesArrangementMapper.getCourseArrangement(term.getYear(), term.getTerm(),String.valueOf(map.get("studentNumber")));
    }



    public String chooseCourses(Map<String, Object> map){
        Term term=jsonUtils.readJson(systemSetting,Term.class);

        coursesSelectionMapper.chooseCourse(term.getYear(), term.getTerm(),String.valueOf(map.get("studentNumber")),String.valueOf(map.get("courseNumber")));
        return "选课成功";
    }

    public List<Map<String,Object>> selectScore(Map<String, Object> map){
        Term term=jsonUtils.readJson(systemSetting,Term.class);
        return coursesSelectionMapper.selectScore(term.getYear(), term.getTerm(),String.valueOf(map.get("studentNo")));
    }

    public Students studentInfo(Map<String, Object> map){
        return studentMapper.studentInfo(String.valueOf(map.get("studentNo")));
    }

    public String deleteCourseSelection(Map<String, Object> map){
        Term term=jsonUtils.readJson(systemSetting,Term.class);
        coursesSelectionMapper.deleteCourseSelection(term.getYear(), term.getTerm(),String.valueOf(map.get("studentNumber")),String.valueOf(map.get("courseNumber")));
        return "退选成功";
    }




    public List<Map<String,Object>> courseInfo(){
        Term term=jsonUtils.readJson(systemSetting,Term.class);
        return coursesInfoMapper.courseInfo(term.getYear(), term.getTerm());
    }

    public String updateStuPassword(Map<String, Object> map){
        studentMapper.updatePassword(String.valueOf(map.get("studentPassword")),String.valueOf(map.get("studentNewPassword")),String.valueOf(map.get("studentNo")));
        return "学生密码修改成功";
    }



    public List<Map<String,Object>> studentClassSelect(Map<String, Object> map){
        Term term=jsonUtils.readJson(systemSetting,Term.class);
        return coursesSelectionMapper.studentClassSelect(term.getYear(), term.getTerm(),String.valueOf(map.get("studentNo")));
    }




    public void save(List<Students> list) {
        for (Students students : list) {
            students.setStudentPassword("000000");
        }
        try {
            studentMapper.insertList(list);
        } catch (Exception e) {
            e.printStackTrace();
            throw new MyException(ExceptionEnums.TABLE_INFO_ERROR);
        }
    }
}
