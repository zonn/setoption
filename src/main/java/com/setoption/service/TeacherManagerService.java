package com.setoption.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.setoption.entity.PageResult;
import com.setoption.enums.ExceptionEnums;
import com.setoption.exception.MyException;
import com.setoption.mapper.qxw_mapper.TeacherMapper;
import com.setoption.pojo.Teachers;
import com.setoption.utils.JwtUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Service
public class TeacherManagerService {
    @Autowired
    private JwtUtil jwtUtil;
    @Resource
    private TeacherMapper teachersMapper;
    //返回教师信息
    public PageResult<Teachers> selectAllTea(Map<String, Object> map) {
        Integer pageNum = Integer.valueOf(map.get("page").toString());
        Integer pageSize = Integer.valueOf(map.get("limit").toString());
        PageHelper.startPage(pageNum, pageSize);
        Example example = new Example(Teachers.class);
        example.excludeProperties("teacherPassword");
        List<Teachers> teachers = teachersMapper.selectByExample(example);
        PageInfo<Teachers> userPageInfo = new PageInfo<Teachers>(teachers);
        return new PageResult<Teachers>(userPageInfo.getTotal(), userPageInfo.getList());
    }
    //添加教师信息
    public int addTeacher(Map<String, Object> map) {
        Boolean bol= String.valueOf(map.get("teacher_onjob")).equals("true");
        Teachers teachers = new Teachers(String.valueOf(map.get("teacher_no")), String.valueOf(map.get("teacher_name")), String.valueOf(map.get("teacher_sex")), Integer.parseInt(map.get("teacher_age").toString()), String.valueOf(map.get("teacher_phone")), bol);
        return teachersMapper.insertSelective(teachers);
    }
    //删除教师信息
    public int delTea(Map<String, Object> map) {
        int result = teachersMapper.deleteByIds(map.get("id").toString());
        if (result == 0) {
            throw new MyException(ExceptionEnums.TABLE_SET_ERROR);
        }
        return result;
    }
    public int resetTeaPassword(Map<String, Object> map) {
        Teachers teachers = new Teachers();
        teachers.setTeacherPassword("000000");
        teachers.setId(Integer.valueOf(map.get("id").toString()));
        int result = teachersMapper.updateByPrimaryKeySelective(teachers);
        if (result == 0) {
            throw new MyException(ExceptionEnums.TABLE_SET_ERROR);
        }
        return result;
    }
    public void save(List<Teachers> list) {
        for (Teachers teachers : list) {
            teachers.setTeacherPassword("000000");
        }
        try {
            teachersMapper.insertList(list);
        } catch (Exception e) {
            e.printStackTrace();
            throw new MyException(ExceptionEnums.TABLE_INFO_ERROR);
        }
    }
}
